openguides (0.84-1) unstable; urgency=medium

  * New upstream release
    - bump dependency on libwiki-toolkit-perl
  * Switch to debhelper 13
  * Add/update homepage links to metacpan
  * Update copyright years

 -- Dominic Hargreaves <dom@earth.li>  Sat, 30 Jan 2021 22:53:58 +0000

openguides (0.82-2) unstable; urgency=medium

  * Add Brazilian Portuguese debconf translation (Closes: #839271)
  * Remove support for Lucy as this is now EoL upstream and is being
    removed from Debian
  * Update Vcs-* fields
  * Update Standards-Version (no changes)
  * Update debhelper compat level
  * Remove possibly vulnerable use of recursive find in postinst
    which was only used in very old upgrade paths (thanks, Lintian)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 09 Sep 2018 23:40:19 +0100

openguides (0.82-1) unstable; urgency=medium

  * New upstream release adding responsive themes (not enabled by
    default for existing installs)
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Sat, 11 Jun 2016 13:12:49 +0100

openguides (0.81-1) unstable; urgency=medium

  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Sun, 24 Jan 2016 16:40:42 +0000

openguides (0.80-1) unstable; urgency=medium

  * Remove obsolete apache-related dependencies (Closes: #791594)
  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Sun, 16 Aug 2015 14:27:25 +0200

openguides (0.79-1) unstable; urgency=medium

  * New upstream release
  * Update Standards-Version (no changes)
  * Add additional Build-Depends on liblucy-perl and libtest-pod-perl

 -- Dominic Hargreaves <dom@earth.li>  Mon, 01 Jun 2015 23:08:53 +0100

openguides (0.76-2) unstable; urgency=medium

  * Add a Depends on liblucy-perl as an alternative to
    libplucene-perl (note that this is still marked as
    Experimental upstream)
  * Add Depends on libmodule-build-perl and libcgi-pm-perl which are
    deprecated from the perl core
  * Fix test suite to work with newer sqlite; thanks to Niko Tyni
    for the patch (Closes: #746125)
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 14 Sep 2014 23:11:04 +0100

openguides (0.76-1) unstable; urgency=low

  * New upstream release
  * Pass "-s /bin/sh" to "su www-data" to cope with the change of www-data's
    shell in base-passwd 3.5.30. Thanks to Colin Watson for the bug report
    and patch (Closes: #734729)

 -- Dominic Hargreaves <dom@earth.li>  Wed, 01 Jan 2014 18:22:34 +0000

openguides (0.74-1) unstable; urgency=low

  * New upstream release
  * Update watch file from qa.debian.org

 -- Dominic Hargreaves <dom@earth.li>  Sat, 31 Aug 2013 11:50:50 +0100

openguides (0.72-1) unstable; urgency=low

  * Add Italian debconf translation (closes: #714012)
  * New upstream release
  * Fix FTBFS with debhelper 9.20130630 by allowing Module::Build
    long options to work

 -- Dominic Hargreaves <dom@earth.li>  Sun, 14 Jul 2013 17:18:44 +0100

openguides (0.71-1) unstable; urgency=low

  * New upstream release
  * remove build_tweaks, now included upstream

 -- Dominic Hargreaves <dom@earth.li>  Sun, 24 Mar 2013 17:04:17 +0000

openguides (0.70-1) experimental; urgency=low

  * New upstream release (closes: #675880, #697184)
    - add new/changed dependencies
    - add static_path and static_url to default config, and add
      NEWS entry about configuring them
    - refresh default wiki.conf
  * Add Vcs-* URLs
  * Switch to minimal debhelper rules file
  * Update Standards-Version (no changes)
  * Use the upstream build system to install extras directly
  * Releasing to experimental as libgeo-coordinates-osgb-perl is
    not yet available in Debian main, and because a wheezy-targetted
    bug-fix release is in unstable

 -- Dominic Hargreaves <dom@earth.li>  Wed, 02 Jan 2013 18:14:25 +0000

openguides (0.65-3) unstable; urgency=low

  * Backport Leaflet mapping support from 0.70 to allow an alternative
    to the deprecated GMaps2 API (closes: #697184)

 -- Dominic Hargreaves <dom@earth.li>  Wed, 02 Jan 2013 17:38:31 +0000

openguides (0.65-2) unstable; urgency=low

  * Add Danish debconf translation (closes: #626639)
  * Switch to dpkg-source 3.0 (quilt) format
  * Update Standards-Version (no changes)
  * Use dh_prep rather than deprecated dh_clean -k
  * Remove the indefinite article from Description (thanks, Lintian)

 -- Dominic Hargreaves <dom@earth.li>  Tue, 17 May 2011 22:17:03 +0100

openguides (0.65-1) unstable; urgency=low

  * New upstream release
  * Add Japanese debconf translation (closes: #510720)
  * Update Standards-Version (no changes)
  * Update debhelper compat version

 -- Dominic Hargreaves <dom@earth.li>  Sun, 28 Feb 2010 16:23:15 +0000

openguides (0.64-1) unstable; urgency=low

  * New upstream release
  * Add Spanish debconf translation (closes: #510337)
  * Fix dependency on Module::Build to allow for the version in Perl 5.10
    (thanks, lintian)

 -- Dominic Hargreaves <dom@earth.li>  Mon, 23 Feb 2009 13:48:44 +0000

openguides (0.63-1) experimental; urgency=low

  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Sun, 17 Aug 2008 00:13:14 +0100

openguides (0.62-3) unstable; urgency=low

  * Add Russian debconf translation (closes: 495128)
  * Add Google Analytics-related fixes from upstream SVN, to avoid
    a latent bug caused by using a deprecated remote interface

 -- Dominic Hargreaves <dom@earth.li>  Sat, 16 Aug 2008 21:17:02 +0100

openguides (0.62-2) unstable; urgency=low

  * Add Swedish debconf translation (closes: #487599)
  * Only try and set up databases in postinst if this is a new install
  * Provide libwiki-toolkit-perl upgrade hook script
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 13 Jul 2008 19:36:26 +0100

openguides (0.62-1) unstable; urgency=low

  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Sun,  8 Jun 2008 12:09:30 +0100

openguides (0.61-4) unstable; urgency=low

  * Don't install /usr/lib/perl5/auto/OpenGuides/.packlist
    (fixes lintian error)

 -- Dominic Hargreaves <dom@earth.li>  Sun,  2 Mar 2008 18:53:43 +0000

openguides (0.61-3) unstable; urgency=low

  * Add Dutch PO file (closes: #449407)
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Tue, 04 Dec 2007 18:50:32 +0000

openguides (0.61-2) unstable; urgency=low

  * Fix clean stage to allow repeated builds (closes: #442695)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 14 Oct 2007 18:19:15 +0100

openguides (0.61-1) unstable; urgency=low

  * New upstream release
  * Fix maintainer scripts to work correctly for initial installs
  * Move NEWS file to correct location

 -- Dominic Hargreaves <dom@earth.li>  Thu, 05 Jul 2007 21:57:58 +0100

openguides (0.60-1) unstable; urgency=low

  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Sun, 13 May 2007 11:47:44 +0100

openguides (0.59-1) unstable; urgency=low

  * New upstream release
  * Add Portugese PO file (closes: #414920)
  * Add UPGRADING to docs

 -- Dominic Hargreaves <dom@earth.li>  Sun, 25 Mar 2007 18:45:19 +0100

openguides (0.58-1) unstable; urgency=low

  * New upstream release
  * Correct name of Czech PO file so it gets used (closes: #404164)
  * Add German PO file (closes: #402423)

 -- Dominic Hargreaves <dom@earth.li>  Fri, 22 Dec 2006 11:58:29 +0000

openguides (0.57-3) unstable; urgency=low

  * Add Czech translation (closes: #396713)

 -- Dominic Hargreaves <dom@earth.li>  Thu, 16 Nov 2006 15:40:24 +0000

openguides (0.57-2) unstable; urgency=low

  * Update default config file to work without Search::InvertedIndex and add
    new variables (closes: #389078)
  * Add French debconf translation (closes: #389042)
  * Move database dependencies to Depends rather than Suggests so a database
    is always available (and since we don't need to support woody any more)
  * Don't invent packages! Postgres dependency is on libdbd-pg-perl, not
    libdbd-pgsql-perl.

 -- Dominic Hargreaves <dom@earth.li>  Tue, 26 Sep 2006 23:16:32 +0100

openguides (0.57-1) unstable; urgency=low

  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Tue, 12 Sep 2006 20:22:49 +0100

openguides (0.56-2) unstable; urgency=low

  * Remove dependency on libsearch-invertedindex-perl
  * Include README.GMAPS as documentation
    (http://dev.openguides.org/ticket/117)
  * Build-dep on libtest-html-content-perl
    (http://dev.openguides.org/ticket/128)
  * Run database setup scripts as user www-data to allow an ident-based
    Postgres setup to work (http://dev.openguides.org/ticket/111)
  * Upload targetted at Debian (closes: #280313)
  * Add lintian override for config file permissions

 -- Dominic Hargreaves <dom@earth.li>  Mon, 28 Aug 2006 18:33:51 +0100

openguides (0.56-1) unstable; urgency=low

  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Wed, 14 Jun 2006 22:34:11 +0100

openguides (0.55-1) unstable; urgency=low

  * New upstream release
  * Add debconf question to prompt for abort, as this upgrade is
    risky

 -- Dominic Hargreaves <dom@earth.li>  Wed, 14 Jun 2006 15:48:56 +0100

openguides (0.54-01-1) unstable; urgency=low

  * New upstream (developer) release.
    * Replaces all dependencies to CGI::Wiki::* with Wiki::Toolkit::*

 -- Dominic Hargreaves <dom@earth.li>  Tue, 16 May 2006 20:01:33 +0100

openguides (0.54-2) unstable; urgency=low

  * By default, install libplucene-perl rather than
    libsearch-invertedindex-perl and Apache 2 rather than Apache 1.
  * Update Standards-Version (no changes).
  * Don't use deprecated chown method in postinst.

 -- Dominic Hargreaves <dom@earth.li>  Fri, 12 May 2006 09:01:01 +0100

openguides (0.54-1) unstable; urgency=low

  * New upstream release.

 -- Dominic Hargreaves <dom@earth.li>  Fri, 21 Apr 2006 10:29:10 +0100

openguides (0.53-1) unstable; urgency=low

  * New upstream release.

 -- Dominic Hargreaves <dom@earth.li>  Thu, 20 Apr 2006 19:07:01 +0000

openguides (0.52-1) unstable; urgency=low

  * New upstream release.

 -- Dominic Hargreaves <dom@earth.li>  Mon,  6 Mar 2006 10:57:45 +0000

openguides (0.51-1) unstable; urgency=low

  * New upstream release.

 -- Dominic Hargreaves <dom@earth.li>  Tue, 15 Nov 2005 17:41:42 +0000

openguides (0.50-1) unstable; urgency=low

  * New upstream release.

 -- Dominic Hargreaves <dom@earth.li>  Sun,  2 Oct 2005 23:42:04 +0100

openguides (0.49-1) unstable; urgency=low

  * New upstream release (no functional changes in the Debian package)
  * Fix lintian warning about obsolete chown usage

 -- Dominic Hargreaves <dom@earth.li>  Thu, 11 Aug 2005 19:00:33 +0000

openguides (0.48-1) unstable; urgency=low

  * New upstream release.
  * Update Standards-Version (no changes).

 -- Dominic Hargreaves <dom@earth.li>  Sun, 24 Jul 2005 00:11:18 +0000

openguides (0.47-4) unstable; urgency=low

  * Fix default permissions of wiki.conf, since it is likely to contain
    passwords.
  * Change sqlite build-dep to sqlite3
  * Add build-dep on libgeo-coordinates-perl to provide more complete test
    coverage.

 -- Dominic Hargreaves <dom@earth.li>  Sun,  8 May 2005 00:01:42 +0100

openguides (0.47-3) unstable; urgency=low

  * Add note regarding choice of SQLite libraries to README.Debian. 

 -- Dominic Hargreaves <dom@earth.li>  Mon, 11 Apr 2005 00:56:51 +0100

openguides (0.47-2) unstable; urgency=low

  * Fix typos and grammatical errors in README.Debian.
  * Remove libfile-spec-perl dependency, as File::Spec is provided by
    the core perl modules, and this packages has been removed from the
    archive.
  * Add some missing build dependencies.

 -- Dominic Hargreaves <dom@earth.li>  Tue,  5 Apr 2005 02:08:59 +0100

openguides (0.47-1) unstable; urgency=low

  * New upstream release.
  * Add .htaccess to try and protect wiki.conf.

 -- Dominic Hargreaves <dom@earth.li>  Sat, 15 Jan 2005 14:37:15 +0000

openguides (0.46-3) unstable; urgency=low

  * Add Plucene support.
  * Fix reindex.pl bug (in examples/).
  * Revert built-in below since it breaks tests.

 -- Dominic Hargreaves <dom@earth.li>  Mon,  3 Jan 2005 01:51:12 +0000

openguides (0.46-2) unstable; urgency=low

  * Set built-in config default for use_plucene to 0.
  * Fix bug in recognising MySQL modules in openguides-setup-db and
    update it to use OpenGuides::Config.

 -- Dominic Hargreaves <dom@earth.li>  Wed, 22 Dec 2004 00:54:25 +0000

openguides (0.46-1) unstable; urgency=low

  * New upstream release.
  * Add watch file.
  * Add missing build dependencies.

 -- Dominic Hargreaves <dom@earth.li>  Wed, 22 Dec 2004 00:36:59 +0000

openguides (0.45-2) unstable; urgency=low

  * Always depend on libgeo-coordinates-utm-perl.

 -- Dominic Hargreaves <dom@earth.li>  Wed,  1 Dec 2004 01:50:21 +0000

openguides (0.45-1) unstable; urgency=low

  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Wed,  1 Dec 2004 01:15:40 +0000

openguides (0.44-1) unstable; urgency=low

  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Wed, 17 Nov 2004 23:50:05 +0000

openguides (0.43-4) unstable; urgency=low

  * Fix openguides-setup-db to really work when SQLite is not installed.

 -- Dominic Hargreaves <dom@earth.li>  Sat, 23 Oct 2004 13:55:27 +0100

openguides (0.43-3) unstable; urgency=low

  * Removed all references to display_categories which had erroneously
    stayed in the release.

 -- Dominic Hargreaves <dom@earth.li>  Fri, 22 Oct 2004 15:01:11 +0100

openguides (0.43-2) unstable; urgency=low

  * Documentation fixes.

 -- Dominic Hargreaves <dom@earth.li>  Fri, 22 Oct 2004 00:10:23 +0100

openguides (0.43-1) unstable; urgency=low

  * New upstream release

 -- Dominic Hargreaves <dom@earth.li>  Thu, 21 Oct 2004 23:49:40 +0100

openguides (0.42-1) unstable; urgency=low

  * New upstream release
  * Remove default CSS file as it wasn't allowed to be a conffile.
  * Remove dependency on libcgi-wiki-plugin-geocache.

 -- Dominic Hargreaves <dom@earth.li>  Wed, 20 Oct 2004 21:09:44 +0100

openguides (0.41-12) unstable; urgency=low

  * Remove the strict dependency on SQLite.

 -- Dominic Hargreaves <dom@earth.li>  Sun, 10 Oct 2004 16:52:46 +0100

openguides (0.41-11) unstable; urgency=low

  * Remove OpenGuides::Build from distribution.

 -- Dominic Hargreaves <dom@earth.li>  Sun, 10 Oct 2004 14:19:24 +0100

openguides (0.41-10) unstable; urgency=low

  * Improve documentation.

 -- Dominic Hargreaves <dom@earth.li>  Sat,  2 Oct 2004 19:32:36 +0100

openguides (0.41-9) unstable; urgency=low

  * Fix brokenness in previous update.

 -- Dominic Hargreaves <dom@earth.li>  Fri,  1 Oct 2004 01:05:29 +0100

openguides (0.41-8) unstable; urgency=low

  * Fix up SQLite file permissions.
  * Include man page for openguides-setup-db.

 -- Dominic Hargreaves <dom@earth.li>  Fri,  1 Oct 2004 01:01:55 +0100

openguides (0.41-7) unstable; urgency=low

  * Make lintian happy.

 -- Dominic Hargreaves <dom@earth.li>  Fri,  1 Oct 2004 00:04:54 +0100

openguides (0.41-6) unstable; urgency=low

  * Fix bug introduced into postinst.

 -- Dominic Hargreaves <dom@earth.li>  Wed, 29 Sep 2004 00:18:02 +0100

openguides (0.41-5) unstable; urgency=low

  * Update wiki.conf with some more relevant comments.
  * Add README.Debian explaining how to use the package.
  * Fix postinst to run openguides-setup-db on all configs,
    not just the default.
  * Fix dependency on web servers (install apache by default).
  * Add INSTALL to docs for now since it contains useful customisation
    tips.

 -- Dominic Hargreaves <dom@earth.li>  Wed, 29 Sep 2004 00:10:34 +0100

openguides (0.41-4) unstable; urgency=low

  * Fix CGI script permissions.

 -- Dominic Hargreaves <dom@earth.li>  Sun, 26 Sep 2004 01:17:49 +0100

openguides (0.41-3) unstable; urgency=low

  * Fix broken custom templates symlink.
  * Fix dependencies on an httpd and the DBD modules (for now, require
    SQLite and suggest MySQL or PostgreSQL.

 -- Dominic Hargreaves <dom@earth.li>  Sun, 26 Sep 2004 01:17:47 +0100

openguides (0.41-2) unstable; urgency=low

  * Fix broken libfile-spec-file dependency.

 -- Dominic Hargreaves <dom@earth.li>  Sun, 26 Sep 2004 00:57:38 +0100

openguides (0.41-1) unstable; urgency=low

  * Initial Release.

 -- Dominic Hargreaves <dom@earth.li>  Sat, 25 Sep 2004 00:00:54 +0100

